<?php


namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends Controller
{
    /**
     * @Route("/posts", name="news_list")
     */
    public function indexAction(){
        $q = new PostsQuery();
        $firstAuthor = $q->findPK(1);
        return $this->render('@App/post/index.html.twig');
    }

    /**
     * @Route("/posts/{id}", name="news_item", requirements={"id": "[0-9]+"})
     */
    public function showAction(){
        die('123');
    }
}
