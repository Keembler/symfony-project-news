<?php

return [
    'propel' => [
        'database' => [
            'connections' => [
                'news' => [
                    'adapter'    => 'mysql',
                    'classname'  => 'Propel\Runtime\Connection\ConnectionWrapper',
                    'dsn'        => 'mysql:host=localhost;dbname=news',
                    'user'       => 'root',
                    'password'   => '',
                    'attributes' => []
                ]
            ]
        ],
        'runtime' => [
            'defaultConnection' => 'news',
            'connections' => ['news']
        ],
        'generator' => [
            'defaultConnection' => 'news',
            'connections' => ['news']
        ]
    ]
];