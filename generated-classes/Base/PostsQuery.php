<?php

namespace Base;

use \Posts as ChildPosts;
use \PostsQuery as ChildPostsQuery;
use \Exception;
use \PDO;
use Map\PostsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'posts' table.
 *
 *
 *
 * @method     ChildPostsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPostsQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildPostsQuery orderByTextShort($order = Criteria::ASC) Order by the text_short column
 * @method     ChildPostsQuery orderByDate($order = Criteria::ASC) Order by the date column
 * @method     ChildPostsQuery orderByTextFull($order = Criteria::ASC) Order by the text_full column
 * @method     ChildPostsQuery orderByActive($order = Criteria::ASC) Order by the active column
 *
 * @method     ChildPostsQuery groupById() Group by the id column
 * @method     ChildPostsQuery groupByTitle() Group by the title column
 * @method     ChildPostsQuery groupByTextShort() Group by the text_short column
 * @method     ChildPostsQuery groupByDate() Group by the date column
 * @method     ChildPostsQuery groupByTextFull() Group by the text_full column
 * @method     ChildPostsQuery groupByActive() Group by the active column
 *
 * @method     ChildPostsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPostsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPostsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPostsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPostsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPostsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPosts findOne(ConnectionInterface $con = null) Return the first ChildPosts matching the query
 * @method     ChildPosts findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPosts matching the query, or a new ChildPosts object populated from the query conditions when no match is found
 *
 * @method     ChildPosts findOneById(int $id) Return the first ChildPosts filtered by the id column
 * @method     ChildPosts findOneByTitle(string $title) Return the first ChildPosts filtered by the title column
 * @method     ChildPosts findOneByTextShort(string $text_short) Return the first ChildPosts filtered by the text_short column
 * @method     ChildPosts findOneByDate(string $date) Return the first ChildPosts filtered by the date column
 * @method     ChildPosts findOneByTextFull(string $text_full) Return the first ChildPosts filtered by the text_full column
 * @method     ChildPosts findOneByActive(boolean $active) Return the first ChildPosts filtered by the active column *

 * @method     ChildPosts requirePk($key, ConnectionInterface $con = null) Return the ChildPosts by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPosts requireOne(ConnectionInterface $con = null) Return the first ChildPosts matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPosts requireOneById(int $id) Return the first ChildPosts filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPosts requireOneByTitle(string $title) Return the first ChildPosts filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPosts requireOneByTextShort(string $text_short) Return the first ChildPosts filtered by the text_short column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPosts requireOneByDate(string $date) Return the first ChildPosts filtered by the date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPosts requireOneByTextFull(string $text_full) Return the first ChildPosts filtered by the text_full column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPosts requireOneByActive(boolean $active) Return the first ChildPosts filtered by the active column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPosts[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPosts objects based on current ModelCriteria
 * @method     ChildPosts[]|ObjectCollection findById(int $id) Return ChildPosts objects filtered by the id column
 * @method     ChildPosts[]|ObjectCollection findByTitle(string $title) Return ChildPosts objects filtered by the title column
 * @method     ChildPosts[]|ObjectCollection findByTextShort(string $text_short) Return ChildPosts objects filtered by the text_short column
 * @method     ChildPosts[]|ObjectCollection findByDate(string $date) Return ChildPosts objects filtered by the date column
 * @method     ChildPosts[]|ObjectCollection findByTextFull(string $text_full) Return ChildPosts objects filtered by the text_full column
 * @method     ChildPosts[]|ObjectCollection findByActive(boolean $active) Return ChildPosts objects filtered by the active column
 * @method     ChildPosts[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PostsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PostsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'news', $modelName = '\\Posts', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPostsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPostsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPostsQuery) {
            return $criteria;
        }
        $query = new ChildPostsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPosts|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PostsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PostsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPosts A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, title, text_short, date, text_full, active FROM posts WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPosts $obj */
            $obj = new ChildPosts();
            $obj->hydrate($row);
            PostsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPosts|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPostsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PostsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPostsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PostsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PostsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PostsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostsQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostsTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the text_short column
     *
     * Example usage:
     * <code>
     * $query->filterByTextShort('fooValue');   // WHERE text_short = 'fooValue'
     * $query->filterByTextShort('%fooValue%', Criteria::LIKE); // WHERE text_short LIKE '%fooValue%'
     * </code>
     *
     * @param     string $textShort The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostsQuery The current query, for fluid interface
     */
    public function filterByTextShort($textShort = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($textShort)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostsTableMap::COL_TEXT_SHORT, $textShort, $comparison);
    }

    /**
     * Filter the query on the date column
     *
     * Example usage:
     * <code>
     * $query->filterByDate('2011-03-14'); // WHERE date = '2011-03-14'
     * $query->filterByDate('now'); // WHERE date = '2011-03-14'
     * $query->filterByDate(array('max' => 'yesterday')); // WHERE date > '2011-03-13'
     * </code>
     *
     * @param     mixed $date The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostsQuery The current query, for fluid interface
     */
    public function filterByDate($date = null, $comparison = null)
    {
        if (is_array($date)) {
            $useMinMax = false;
            if (isset($date['min'])) {
                $this->addUsingAlias(PostsTableMap::COL_DATE, $date['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($date['max'])) {
                $this->addUsingAlias(PostsTableMap::COL_DATE, $date['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostsTableMap::COL_DATE, $date, $comparison);
    }

    /**
     * Filter the query on the text_full column
     *
     * Example usage:
     * <code>
     * $query->filterByTextFull('fooValue');   // WHERE text_full = 'fooValue'
     * $query->filterByTextFull('%fooValue%', Criteria::LIKE); // WHERE text_full LIKE '%fooValue%'
     * </code>
     *
     * @param     string $textFull The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostsQuery The current query, for fluid interface
     */
    public function filterByTextFull($textFull = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($textFull)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostsTableMap::COL_TEXT_FULL, $textFull, $comparison);
    }

    /**
     * Filter the query on the active column
     *
     * Example usage:
     * <code>
     * $query->filterByActive(true); // WHERE active = true
     * $query->filterByActive('yes'); // WHERE active = true
     * </code>
     *
     * @param     boolean|string $active The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostsQuery The current query, for fluid interface
     */
    public function filterByActive($active = null, $comparison = null)
    {
        if (is_string($active)) {
            $active = in_array(strtolower($active), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(PostsTableMap::COL_ACTIVE, $active, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPosts $posts Object to remove from the list of results
     *
     * @return $this|ChildPostsQuery The current query, for fluid interface
     */
    public function prune($posts = null)
    {
        if ($posts) {
            $this->addUsingAlias(PostsTableMap::COL_ID, $posts->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the posts table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PostsTableMap::clearInstancePool();
            PostsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PostsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PostsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PostsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PostsQuery
